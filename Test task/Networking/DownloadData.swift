//
//  DownloadData.swift
//  Test task
//
//  Created by Bohdan Hodovanets on 6/1/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import Foundation

protocol DownloadDataDelegate {
    func didUpdate(sender: DownloadData)
}

class DownloadData {
   
    var delegate: DownloadDataDelegate?
    var searchResult = [BestMatch] ()
    var dataURL = String()
    var chartResult = [String:TimeSeries5Min]()
    
    
    func getSearchResult() {
        let session = URLSession.shared
        let urlJSON = URL(string: dataURL)
        guard let url = urlJSON else {return}
        let task = session.dataTask(with: url) { (data, response, error) in
            if error != nil || data == nil {
                print("Client error")
                return
            }
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error")
                return
            }
            
            guard let mime = response.mimeType, mime == "application/json" else {
                print("Wrong mime type")
                return
            }
            do {
                let jsonDecoder = JSONDecoder()
                guard let data = data else {return}
                let downloadedData = try jsonDecoder.decode(SearchDataModel.self, from: data)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
                if let downloadedData = downloadedData.bestMatches {
                    self.searchResult = downloadedData
                    self.delegate?.didUpdate(sender: self)
                    
                }
            } catch {
                print("JSON error: \(error.localizedDescription)")
            }
        }
        task.resume()
    }
    
    func getCellResult(url: String) {
        let session = URLSession.shared
        let urlJSON = URL(string: url)
        guard let url = urlJSON else {return}
        let task = session.dataTask(with: url) { (data, response, error) in
            if error != nil || data == nil {
                print("Client error")
                return
            }
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error")
                return
            }
            
            guard let mime = response.mimeType, mime == "application/json" else {
                print("Wrong mime type")
                return
            }
            do {
                let jsonDecoder = JSONDecoder()
                guard let data = data else {return}
                let downloadedChartData = try jsonDecoder.decode(ChartDataModel.self, from: data)
                if let downloadedChartData = downloadedChartData.timeSeries5Min {
                    self.chartResult = downloadedChartData
                    self.delegate?.didUpdate(sender: self)
                }
                
            } catch {
                print("JSON error: \(error.localizedDescription)")
            }
        }
        task.resume()
    }
    
    
}


