//
//  SearchDataModel.swift
//  Test task
//
//  Created by Bohdan Hodovanets on 6/1/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import Foundation

// MARK: - SearchDataModel
struct SearchDataModel: Decodable {
    let bestMatches: [BestMatch]?
}

// MARK: - BestMatch
struct BestMatch {
    let symbol, name, currency: String?
}

extension BestMatch: Decodable {
    
    enum BestMatchKeys: String, CodingKey {
        case symbol = "1. symbol"
        case name = "2. name"
        case currency = "8. currency"
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: BestMatchKeys.self)
        let symbol = try container.decode(String.self, forKey: .symbol)
        let name = try container.decode(String.self, forKey: .name)
        let currency = try container.decode(String.self, forKey: .currency)
        
        self.init(symbol: symbol, name: name, currency: currency)
        
    }
}



