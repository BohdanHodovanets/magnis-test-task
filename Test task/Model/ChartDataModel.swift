//
//  ChartDataModel.swift
//  Test task
//
//  Created by Bohdan Hodovanets on 6/1/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import Foundation

// MARK: - ChartDataModel
struct ChartDataModel: Decodable {
    let timeSeries5Min: [String:TimeSeries5Min]?
    
    enum CodingKeys: String, CodingKey {
        case timeSeries5Min = "Time Series (5min)"
    }
}

// MARK: - TimeSeries

struct TimeSeries5Min {
    let open, high, low, close: String?
    let volume: String?
}

extension TimeSeries5Min: Decodable {
    
    enum TimeSeries5MinKeys: String, CodingKey {
        case open = "1. open"
        case high = "2. high"
        case low = "3. low"
        case close = "4. close"
        case volume = "5. volume"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: TimeSeries5MinKeys.self)
        let open = try container.decode(String.self, forKey: .open)
        let high = try container.decode(String.self, forKey: .high)
        let low = try container.decode(String.self, forKey: .low)
        let close = try container.decode(String.self, forKey: .close)
        let volume = try container.decode(String.self, forKey: .volume)
        
        self.init(open: open, high: high, low: low, close: close, volume: volume)
        
    }
}



