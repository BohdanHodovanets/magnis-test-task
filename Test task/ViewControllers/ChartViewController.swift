//
//  ChartViewController.swift
//  Test task
//
//  Created by Bohdan Hodovanets on 6/1/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import UIKit
import SwiftCharts

class ChartViewController: UIViewController, DownloadDataDelegate{
    
    var downloadChartData = DownloadData()
    var url = String()
    var symbol = String()
    var chart: Chart?
    var chartPoints = [ChartPointCandleStick]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadChartData.delegate = self
        print(url)
        downloadChartData.getCellResult(url: url)
        
    }
    
    func didUpdate(sender: DownloadData) {
        DispatchQueue.main.async {
            self.createChart()
        }
    }
    
    func createChart() {
        let labelSettings = ChartLabelSettings(font: ExamplesDefaults.labelFont)
        var readFormatter = DateFormatter()
        readFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var displayFormatter = DateFormatter()
        displayFormatter.dateFormat = "HH:mm"
        let date = {(str: String) -> Date in
            return readFormatter.date(from: str)!
        }

        let calendar = Calendar.current
        let dateWithComponents = {(hour: Int, minute: Int) -> Date in
            var components = DateComponents()
            components.hour = hour
            components.minute = minute
            return calendar.date(from: components)!
        }

        func filler(_ date: Date) -> ChartAxisValueDate {
            let filler = ChartAxisValueDate(date: date, formatter: displayFormatter)
            filler.hidden = true
            return filler
        }

        for (key,values) in downloadChartData.chartResult {

            chartPoints.append(ChartPointCandleStick(date: date(key), formatter: displayFormatter, high: Double(values.high!) as! Double, low: Double(values.low!) as! Double, open: Double(values.open!) as! Double, close: Double(values.close!) as! Double))
        }
        let yValues = stride(from: 100, through: 300, by: 5).map {ChartAxisValueDouble(Double($0), labelSettings: labelSettings)}

        let xGeneratorDate = ChartAxisValuesGeneratorDate(unit: .minute, preferredDividers:5, minSpace: 1, maxTextSize: 12)

        let xLabelGeneratorDate = ChartAxisLabelsGeneratorDate(labelSettings: labelSettings, formatter: displayFormatter)

        let firstDate = date("2019-05-31 10:50:00")
        let lastDate = date("2019-05-31 16:00:00")

        print("charPoint count: \(chartPoints.count)")
        

        let xModel = ChartAxisModel(firstModelValue: firstDate.timeIntervalSince1970, lastModelValue: lastDate.timeIntervalSince1970, axisTitleLabels: [ChartAxisLabel(text: "Date", settings: labelSettings)], axisValuesGenerator: xGeneratorDate, labelsGenerator: xLabelGeneratorDate)

        let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: symbol, settings: labelSettings.defaultVertical()))
        let chartFrame = ExamplesDefaults.chartFrame(view.bounds)
        let chartSettings = ExamplesDefaults.chartSettings
        let coordsSpace = ChartCoordsSpaceRightBottomSingleAxis(chartSettings: chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
        let (xAxisLayer, yAxisLayer, innerFrame) = (coordsSpace.xAxisLayer, coordsSpace.yAxisLayer, coordsSpace.chartInnerFrame)
        let chartPointsLineLayer = ChartCandleStickLayer<ChartPointCandleStick>(xAxis: xAxisLayer.axis, yAxis: yAxisLayer.axis, chartPoints: chartPoints, itemWidth: Env.iPhone ? 10 : 5, strokeWidth: Env.iPhone ? 1 : 0.6, increasingColor: UIColor.green, decreasingColor: UIColor.red)
        let settings = ChartGuideLinesLayerSettings(linesColor: UIColor.black, linesWidth: ExamplesDefaults.guidelinesWidth)
        let guidelinesLayer = ChartGuideLinesLayer(xAxisLayer: xAxisLayer, yAxisLayer: yAxisLayer, settings: settings)
        let dividersSettings =  ChartDividersLayerSettings(linesColor: UIColor.black, linesWidth: ExamplesDefaults.guidelinesWidth, start: Env.iPhone ? 7 : 3, end: 0)
        let dividersLayer = ChartDividersLayer(xAxisLayer: xAxisLayer, yAxisLayer: yAxisLayer, settings: dividersSettings)

        let chart = Chart(
            frame: chartFrame,
            innerFrame: innerFrame,
            settings: chartSettings,
            layers: [
                xAxisLayer,
                yAxisLayer,
                guidelinesLayer,
                dividersLayer,
                chartPointsLineLayer
            ]
        )

        view.addSubview(chart.view)
        self.chart = chart
    }
    
}


