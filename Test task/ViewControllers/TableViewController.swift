//
//  TableViewController.swift
//  Test task
//
//  Created by Bohdan Hodovanets on 6/1/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController, UISearchBarDelegate, DownloadDataDelegate {
    
    // MARK: - Properties
    let downloadData = DownloadData()
    var searchBar = UISearchBar()
    var settingBarItem = UIBarButtonItem()
    var shoudlShowResult = false
    var dataURL = String() //APIkeys = FDWSHLS4BMAUVH0K, 54P8ICUR3MYZXCL9
    var urlForChart = ""
    var symbolChart = ""
    var interval = "interval=5min&"
    var function = "TIME_SERIES_INTRADAY"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createSettingBarButton()
        createSearchBar()
        downloadData.delegate = self
        tableView.reloadData()
    }
    
    
    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shoudlShowResult != false {
            return downloadData.searchResult.count
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! CustomCell
        cell.symbolLabel.text = downloadData.searchResult[indexPath.row].symbol
        cell.nameLabel.text = downloadData.searchResult[indexPath.row].name
        cell.currencyLabel.text = downloadData.searchResult[indexPath.row].currency
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let chartViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChartViewController") as! ChartViewController
        searchBar.endEditing(true)
        let currentCell = downloadData.searchResult[indexPath.row].symbol
        if let url = currentCell {
            downloadData.dataURL =  "https://www.alphavantage.co/query?function=\(function)&symbol=" + url + "&\(interval)apikey=FDWSHLS4BMAUVH0K"
            urlForChart = downloadData.dataURL
            symbolChart = url
        }
        chartViewController.url = urlForChart
        chartViewController.symbol = symbolChart
        self.navigationController?.pushViewController(chartViewController, animated: true)
       
    }
    
            
    // MARK: - Table view reload method
    func didUpdate(sender: DownloadData) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    // MARK: - BarButton
    func createSettingBarButton() {
        settingBarItem = UIBarButtonItem(title: "Setting", style: .plain, target: self, action: #selector(clickedSettingBarItem(_:)))
        self.navigationItem.leftItemsSupplementBackButton = true
        self.navigationItem.leftBarButtonItem = settingBarItem
    }
    
    @objc func clickedSettingBarItem(_ sender: UIBarButtonItem) {
        self.alert(title: "Timeframe preference", message: "Select timeframe", style: .actionSheet)
    }
    
    //MARK: - Alert sheet
    func alert(title: String, message: String, style: UIAlertController.Style) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        let action1 = UIAlertAction(title: "5 min", style: UIAlertAction.Style.default) { (action) in
            self.interval = "interval=5min&"
            self.function = "TIME_SERIES_INTRADAY"
        }
        let action2 = UIAlertAction(title: "30 min", style: UIAlertAction.Style.default) { (action) in
            self.interval = "interval=30min&"
            self.function = "TIME_SERIES_INTRADAY"
        }
        let action3 = UIAlertAction(title: "1 hour", style: UIAlertAction.Style.default) { (action) in
            self.interval = "interval=60min&"
            self.function = "TIME_SERIES_INTRADAY"
        }
        let action4 = UIAlertAction(title: "1 day", style: UIAlertAction.Style.default) { (action) in
            self.interval = ""
            self.function = "TIME_SERIES_DAILY"
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in }
        alertController.addAction(actionCancel)
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(action3)
        alertController.addAction(action4)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Search bar
    func createSearchBar() {
        searchBar.placeholder = "AAPL, Micro and ect."
        searchBar.tintColor = UIColor.blue
        searchBar.sizeToFit()
        searchBar.tintColor = UIColor.black
        navigationItem.titleView = searchBar
        navigationController?.title = "Search"
        searchBar.delegate = self
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != "" {
            shoudlShowResult = true
            let searchKey = String(searchText)
            downloadData.dataURL = "https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=" + searchKey + "&apikey=FDWSHLS4BMAUVH0K"
            downloadData.getSearchResult()
            tableView.reloadData()
        } else {
            shoudlShowResult = false
            tableView.reloadData()
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = true
        settingBarItem.title = ""
        return true
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = false
        settingBarItem.title = "Setting"
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
