//
//  Env.swift
//  Test task
//
//  Created by Bohdan Hodovanets on 6/3/19.
//  Copyright © 2019 Bohdan Hodovanets. All rights reserved.
//

import UIKit

class Env {
    static var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
}
